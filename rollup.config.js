import { terser } from 'rollup-plugin-terser';
import babel from 'rollup-plugin-babel';

const config = {
  input: 'src/index.js',
  output: [{
    format: 'umd',
    file: 'dist/index.js',
    name: 'index',
    globals: {
      react: 'React',
    }
  },
  {
    file: 'dist/index.esm.js',
    format: 'esm'
  }],
  external: ['react'],
  plugins: [
    babel({ exclude: 'node_modules/**', }),
    terser(),
  ],
};

export default config;
